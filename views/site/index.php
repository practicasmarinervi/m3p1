<?php

/* @var $this yii\web\View */

$this->title = 'Practica 1';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>EDITORIAL</h1>

        <p class="lead">Te mostramos nuestros Libros y Autores</p>

        </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
                <h2>Lecturas variadas</h2>
               
            </div>
            <div class="col-lg-4">
                <h2>Para tus ratos de ocio</h2>
              
            </div>
            <div class="col-lg-4">
                <h2>Para tu tiempo libre</h2>
           
            </div>
        </div>
        
        <div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title">Bienvenidos a nuestra Editorial</h3>
  </div>
  <div class="panel-body">
    En nuestro menu obtendras un listado tanto de nuestros libros como de nuestros autores
  </div>
</div>

    </div>
</div>
