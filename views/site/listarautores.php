
<?php


use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

 ?>   

<div class="panel panel-default">
    
  <div class="panel-heading">
    <h3 class="panel-title"><?=$titulopanel?></h3>
  </div>
    
  <div class="panel-body">
     <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'nombre',
            'apellido'

        ],
         'summary'=>false
    ]);
    
    ?>
  </div>
</div>

   